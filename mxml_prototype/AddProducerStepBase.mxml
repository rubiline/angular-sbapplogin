<?xml version="1.0"?>
<wizard:Step xmlns:fx="http://ns.adobe.com/mxml/2009"
             xmlns:wizard="wizard.*"
             xmlns:s="library://ns.adobe.com/flex/spark"
             xmlns:c="library://ns.cleartoolkit.com/flex/clear"
             initialize="this_initializeHandler(event)"
             viewActivate="this_viewActivateHandler(event)"
             sidebarVisible="false"
             visible="{!autoAddInProgress}"
             keyUp="this_keyUpHandler(event)">

    <fx:Declarations>
        <fx:Array id="navigationContentElements">
            <c:Button id="previousButton"
                      glyph="{IG.PREVIOUS}"
                      label="Back"
                      toolTip=""
                      click="goToPreviousStepHandler()"
                      enabled="{previousButtonEnabled}"
                      visible="{showPreviousButton}"
                      includeInLayout="{showPreviousButton}"
                      tabIndex="{firstButtonTabIndex}"/>

            <c:Button id="cancelButton"
                      styleName="cancelButton32"
                      glyph="{IG.CANCEL}"
                      label="Cancel"
                      toolTip=""
                      click="cancelHandler()"
                      enabled="{cancelButtonEnabled}"
                      visible="{isProducerFlow ? model.shortBgaLink &amp;&amp; showCancelButton : showCancelButton}"
                      includeInLayout="{isProducerFlow ? model.shortBgaLink &amp;&amp; showCancelButton : showCancelButton}"
                      tabIndex="{thirdButtonTabIndex}"/>

            <s:Spacer width="100%"/>

            <c:Button id="nextButton"
                      glyph="{IG.NEXT}" glyphPlacement="right"
                      label="Next"
                      toolTip=""
                      click="goToNextStepHandler()"
                      enabled="{nextButtonEnabled}"
                      visible="{showNextButton}"
                      includeInLayout="{showNextButton}"
                      tabIndex="{secondButtonTabIndex}"/>
        </fx:Array>

        <s:RichText id="contactYourBgaRichText"
                    backgroundAlpha="1" backgroundColor="#FFFFFF"
                    paddingBottom="10" paddingRight="10"
                    paddingLeft="10" paddingTop="10"
                    width="100%" height="100%">
            <s:p>Contact your BGA if any of the <s:br/> pre-filled information is incorrect.</s:p>
        </s:RichText>

    </fx:Declarations>

    <wizard:layout>
        <s:VerticalLayout gap="10"/>
    </wizard:layout>

    <s:Group width="100%" mxmlContent="{stepContent}"/>
    <s:HGroup id="navigationContentGroup" width="100%" horizontalAlign="right"
            paddingRight="10" paddingLeft="10"/>

    <fx:Script>
        <![CDATA[
        import com.farata.controls.MessageBox;
        import com.farata.dashboard.security.SecurityContext;
        import com.farata.dto.LoginSessionDTO;
        import com.farata.dto.ModelBgaDTO;
        import com.farata.utils.StringUtils;

        import graphics.IG;

        import mx.events.FlexEvent;

        import spark.events.ViewNavigatorEvent;

        import view.addProducer2.AddProducerWorkflow;
        import view.addProducer2.supportClasses.AddProducerController;

        //------------------------------------------------------------------
        //
        //  Variables
        //
        //------------------------------------------------------------------

        [Bindable]
        [ArrayElementType("mx.core.IVisualElement")]
        public var stepContent:Array;

        [Bindable]
        public var showPreviousButton:Boolean;
        [Bindable]
        public var previousButtonEnabled:Boolean = true;

        [Bindable]
        public var showNextButton:Boolean;
        [Bindable]
        public var nextButtonEnabled:Boolean = true;

        [Bindable]
        public var showCancelButton:Boolean;
        [Bindable]
        public var cancelButtonEnabled:Boolean = true;

        protected const firstControlTabIndex:int = 90;
        protected const secondControlTabIndex:int = 91;
        protected const thirdControlTabIndex:int = 92;

        protected const firstButtonTabIndex:int = 100;
        protected const secondButtonTabIndex:int = 101;
        protected const thirdButtonTabIndex:int = 102;

        private var _processingAutoAdd:Boolean = false;

        //------------------------------------------------------------------
        //
        //  Properties
        //
        //------------------------------------------------------------------

        [Bindable("dataChange")]
        public function get addProducerWizard():AddProducerWorkflow {
            return data ? data.addProducerWizard : null;
        }

        [Bindable("dataChange")]
        public function get model():LoginSessionDTO {
            return addProducerWizard ? addProducerWizard.model : null;
        }

        [Bindable("dataChange")]
        public function get controller():AddProducerController {
            return addProducerWizard ? addProducerWizard.controller : null;
        }

        [Bindable("dataChange")]
        public function get gaDTO():ModelBgaDTO {
            return addProducerWizard ? addProducerWizard.gaDTO : null;
        }

        public function get securityContext():SecurityContext {
            return SecurityContext.instance;
        }

        [Bindable("dataChange")]
        [Bindable("processingAutoAddChange")]
        public function get autoAddInProgress():Boolean {
            return addProducerWizard.isAutoAdd && _processingAutoAdd;
        }

        [ArrayElementType("mx.core.IVisualElement")]
        protected function get navigationControls():Array {
            return navigationContentElements;
        }

        protected function get isAutoAddAllowed():Boolean {
            return false;
        }

        [Bindable("dataChange")]
        protected function get isProducerFlow():Boolean {
            return addProducerWizard ? addProducerWizard.isProducerFlow : false;
        }

        [Bindable("dataChange")]
        protected function get isAutoAddProducerFlowWithGuid():Boolean {
            return addProducerWizard ? addProducerWizard.isAutoAddProducerFlowWithGuid : false;
        }

        [Bindable]
        protected function get skipAddContactDataStep():Boolean {
            return isAutoAddProducerFlowWithGuid && !model.branchEditable && (!model.branchVisible || (model.branchVisible && StringUtils.isEmpty(model.branchValue)));
        }

        [Bindable("dataChange")]
        protected function get originModel():LoginSessionDTO {
            return addProducerWizard ? addProducerWizard.originModel : null;
        }

        //------------------------------------------------------------------
        //
        //  This Handlers
        //
        //------------------------------------------------------------------

        protected function this_initializeHandler(event:FlexEvent):void {
            navigationContentGroup.mxmlContent = navigationControls;
        }

        protected function this_viewActivateHandler(event:ViewNavigatorEvent):void {
            if (addProducerWizard.isAutoAdd && isAutoAddAllowed) {
                startAutoAddProcess();
            }
            updateEnableForControlsWithSSOValue();
        }

        protected function this_keyUpHandler(event:KeyboardEvent):void {
            switch (event.keyCode) {
                case Keyboard.ENTER:
                    if (nextButton && nextButton.visible && nextButton.enabled && (event.target != cancelButton) && (event.target != previousButton)) {
                        goToNextStepHandler();
                    }
                    break;
            }
        }

        //------------------------------------------------------------------
        //
        //  UI Handlers
        //
        //------------------------------------------------------------------

        protected function goToNextStepHandler():void {
            if (addProducerWizard) {
                addProducerWizard.goToNextStep();
            }
        }

        protected function goToPreviousStepHandler():void {
            if (addProducerWizard) {
                addProducerWizard.goToPreviousStep();
            }
        }

        protected function goToStep(stepName:String):void {
            if (addProducerWizard) {
                addProducerWizard.goToStep(stepName);
            }
        }

        protected function cancelHandler():void {
            MessageBox.show(this,
                    "Are you sure you want to cancel producer creation process?\n\nAll your progress will be lost\n",
                    "Confirmation",
                    MessageBox.YESNO | MessageBox.ICONQUESTION,
                    cancelWorkflowHandler);
        }

        protected function cancelWorkflowHandler(code:int, param:Object = null):void {
            if (code == MessageBox.YES) {
                if (addProducerWizard) {
                    addProducerWizard.cancel();
                }
            }
        }

        //------------------------------------------------------------------
        //
        //  Methods
        //
        //------------------------------------------------------------------

        protected function canEditSSOParameter(paramName:String):Boolean {
            var result:Boolean = true;
            if (isProducerFlow) {
                var originValue:String = originModel.hasOwnProperty(paramName) ? originModel[paramName] : "";
                result = StringUtils.isEmptyOrWhitespace(originValue);
                if (paramName == "branchValue") {
                    result = model.branchEditable || result;
                }
            }
            return result;
        }

        protected function updateEnableForControlsWithSSOValue():void {
            //Override in AddProducerWorkflow Steps
        }

        protected function startAutoAddProcess():void {
            _processingAutoAdd = true;
            dispatchEvent(new Event("processingAutoAddChange"));
        }

        protected function stopAutoAddProcess():void {
            _processingAutoAdd = false;
            dispatchEvent(new Event("processingAutoAddChange"));
        }

        ]]>
    </fx:Script>
</wizard:Step>
