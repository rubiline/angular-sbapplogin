package view.addProducer2.supportClasses {
import clear.events.DynamicEvent;
import clear.rpc.remoting.mxml.RemoteObject;

import com.farata.ams.AMS;
import com.farata.ams.AMSFactory;
import com.farata.core.ApplicationError;
import com.farata.dashboard.security.SecurityContext;
import com.farata.dto.GA2ProducerDTO;
import com.farata.dto.LoginSessionDTO;
import com.farata.dto.ModelBgaDTO;
import com.farata.dto.ModelProducerDTO;
import com.farata.dto.ProducerVeryShortInfoDTO;
import com.farata.dto.UserInfoDTO;
import com.farata.managers.ApplicationErrorManager;
import com.farata.utils.StringUtils;
import com.farata.webservice.NiprHitListService;
import com.surancebay.crm.utils.RemotingUtils;

import flash.events.Event;
import flash.events.EventDispatcher;

import mx.collections.ArrayCollection;

import mx.logging.ILogger;
import mx.logging.Log;
import mx.rpc.AsyncResponder;
import mx.rpc.AsyncToken;
import mx.rpc.events.FaultEvent;
import mx.rpc.events.ResultEvent;

[Event(name="searchProducerInNiprResult", type="clear.events.DynamicEvent")]
[Event(name="searchProducerInNiprFault", type="clear.events.DynamicEvent")]

[Event(name="isProducerExistResult", type="clear.events.DynamicEvent")]
[Event(name="isProducerExistFault", type="clear.events.DynamicEvent")]

[Event(name="isProducerLinkedToBgaResult", type="clear.events.DynamicEvent")]
[Event(name="isProducerLinkedToBgaFault", type="clear.events.DynamicEvent")]

[Event(name="hasPDBReportResult", type="clear.events.DynamicEvent")]
[Event(name="hasPDBReportFault", type="clear.events.DynamicEvent")]

[Event(name="loadPdbReportResult", type="clear.events.DynamicEvent")]
[Event(name="loadPdbReportFault", type="clear.events.DynamicEvent")]

[Event(name="loadAmsPersonalInfoResult", type="clear.events.DynamicEvent")]
[Event(name="loadAmsPersonalInfoFault", type="clear.events.DynamicEvent")]

[Event(name="addProducerResult", type="clear.events.DynamicEvent")]
[Event(name="addProducerFault", type="clear.events.DynamicEvent")]

[Event(name="paymentResponsibilityResult", type="clear.events.DynamicEvent")]
[Event(name="paymentResponsibilityFault", type="clear.events.DynamicEvent")]


[Event(name="contractsCheckResult", type="clear.events.DynamicEvent")]
[Event(name="contractsCheckFault", type="clear.events.DynamicEvent")]

[Event(name="updateProducerInfoTransactionCostResult", type="clear.events.DynamicEvent")]
[Event(name="updateProducerInfoTransactionCostFault", type="clear.events.DynamicEvent")]

public class AddProducerController extends EventDispatcher {

    //------------------------------------------------------------------
    //
    //  Class Constants
    //
    //------------------------------------------------------------------

    public static const CAN_NOT_CHECK_YOUR_IDENTITY_ERROR:String = "We couldn't check your identity as our services are unavailable right now. Please try again later.";
    public static const CAN_NOT_LOAD_PDB_REPORT:String = "We couldn't load PDB report as " + NiprHitListService.SERVICES_UNAVAILABLE_ERROR;
    public static const CAN_NOT_RETRIEVE_PAYMENT_RESPONSIBILITY:String = "We couldn't retrieve payment responsibility as our services are unavailable right now. Please try again later.";
    public static const CAN_NOT_RETRIEVE_PAYMENT_DETAILS:String = "We couldn't retrieve payment details as our services are unavailable right now. Please try again later.";
    public static const PRODUCER_LICENSE_EXPECTED_ERROR:String = "You have entered a firm's license data. To register yourself, you have to enter your own license data.";
    public static const PRODUCER_LICENSE_EXPECTED_ERROR_FOR_ADMIN:String = "You have entered a firm's license data. To register the agent, you have to enter their own individual license data.";


    public static const SEARCH_PRODUCER_IN_NIPR_RESULT:String = "searchProducerInNiprResult";
    public static const SEARCH_PRODUCER_IN_NIPR_FAULT:String = "searchProducerInNiprFault";

    public static const CHECK_PRODUCER_SSN_IN_NIPR_RESULT:String = "checkProducerSSNInNiprResult";
    public static const CHECK_PRODUCER_SSN_IN_NIPR_FAULT:String = "checkProducerSSNInNiprFault";

    public static const IS_PRODUCER_EXIST_RESULT:String = "isProducerExistResult";
    public static const IS_PRODUCER_EXIST_FAULT:String = "isProducerExistFault";

    public static const IS_PRODUCER_LINKED_TO_BGA_RESULT:String = "isProducerLinkedToBgaResult";
    public static const IS_PRODUCER_LINKED_TO_BGA_FAULT:String = "isProducerLinkedToBgaFault";

    public static const HAS_PDB_REPORT_RESULT:String = "hasPDBReportResult";
    public static const HAS_PDB_REPORT_FAULT:String = "hasPDBReportFault";

    public static const LOAD_PDB_REPORT_RESULT:String = "loadPdbReportResult";
    public static const LOAD_PDB_REPORT_FAULT:String = "loadPdbReportFault";

    public static const LOAD_AMS_PERSONAL_INFO_RESULT:String = "loadAmsPersonalInfoResult";
    public static const LOAD_AMS_PERSONAL_INFO_FAULT:String = "loadAmsPersonalInfoFault";

    public static const ADD_PRODUCER_RESULT:String = "addProducerResult";
    public static const ADD_PRODUCER_FAULT:String = "addProducerFault";

    public static const PAYMENT_RESPONSIBILITY_RESULT:String = "paymentResponsibilityResult";
    public static const PAYMENT_RESPONSIBILITY_FAULT:String = "paymentResponsibilityFault";


    public static const CONTRACTS_CHECK_RESULT:String = "contractsCheckResult";
    public static const CONTRACTS_CHECK_FAULT:String = "contractsCheckFault";

    public static const PROCEED_WITH_APP_REQ_RESULT:String = "proceedWithAppointmentRequestResult";
    public static const PROCEED_WITH_APP_REQ_FAULT:String = "proceedWithAppointmentRequestFault";

    public static const UPDATE_PRODUCER_INFO_TRANSACTION_COST_RESULT:String = "updateProducerInfoTransactionCostResult";
    public static const UPDATE_PRODUCER_INFO_TRANSACTION_COST_FAULT:String = "updateProducerInfoTransactionCostFault";

    protected static const SEARCH_PRODUCER_REASON:String = "searchProducer";
    protected static const CHECK_PRODUCER_SNN_REASON:String = "checkProducerSSN";

    private static const LOG:ILogger = Log.getLogger("AddProducerController");

    //------------------------------------------------------------------
    //
    //  Variables
    //
    //------------------------------------------------------------------

    public function AddProducerController() {
        super();
    }

    public var isAddingImportedProducer:Boolean = false;

    private var _npn:String = "";
    private var _fullName:String = "";
    private var _dateOfBirth:String = "";
    private var _niprHitlistService:NiprHitListService = null;
    private var _reasonToLoadInfoFromNipr:String = "";
    private var _npnService:RemoteObject = null;
    private var _ga2producerService:RemoteObject = null;
    private var _pdbService:RemoteObject = null;
    private var _loadPdbReportService:RemoteObject = null;
    private var _producerService:RemoteObject = null;
    private var _paymentResponsibilityService:RemoteObject = null;
    private var _transactionCostService:RemoteObject = null;

    private var _captiveProducerModel:ModelProducerDTO;

    //------------------------------------------------------------------
    //
    //  Properties
    //
    //------------------------------------------------------------------

    private var _inProgress:Boolean = false;

    [Bindable("inProgressChange")]
    public function get inProgress():Boolean {
        return _inProgress;
    }

    private var _progressMessage:String = "";

    [Bindable("inProgressChange")]
    public function get progressMessage():String {
        return _progressMessage;
    }

    private var _isProducerExist:Boolean = false;

    [Bindable("isProducerExistResult")]
    [Bindable("isProducerExistFault")]
    public function get isProducerExist():Boolean {
        return _isProducerExist;
    }

    private var _isProducerLinkedToBga:Boolean = false;

    [Bindable("isProducerLinkedToBgaResult")]
    [Bindable("isProducerLinkedToBgaFault")]
    public function get isProducerLinkedToBga():Boolean {
        return _isProducerLinkedToBga;
    }

    private var _hasPdbReport:Boolean = false;

    [Bindable("hasPDBReportResult")]
    [Bindable("hasPDBReportFault")]
    public function get hasPdbReport():Boolean {
        return _hasPdbReport;
    }

    private var _amsPersonalInfo:ModelProducerDTO = null;

    [Bindable("loadAmsPersonalInfoResult")]
    [Bindable("loadAmsPersonalInfoFault")]
    public function get amsPersonalInfo():ModelProducerDTO {
        return _amsPersonalInfo;
    }

    private var _userInfo:UserInfoDTO;

    [Bindable("addProducerResult")]
    [Bindable("addProducerFault")]
    public function get userInfo():UserInfoDTO {
        return _userInfo;
    }

    private var _producerDTO:ModelProducerDTO;

    [Bindable("addProducerResult")]
    [Bindable("addProducerFault")]
    public function get producerDTO():ModelProducerDTO {
        return _producerDTO;
    }

    private var _producerId:Number;

    [Bindable("isProducerExistResult")]
    [Bindable("isProducerExistFault")]
    public function get producerId():Number {
        return _producerId;
    }

    [Bindable("searchProducerInNiprResult")]
    [Bindable("searchProducerInNiprFault")]
    public function get npnFromNipr():String {
        return _npn;
    }

    private var _ssn:String;
    public function get ssn():String {
        return _ssn;
    }

    private var _licenseNoValue:String;
    public function get licenseNoValue():String {
        return _licenseNoValue;
    }

    [Bindable("searchProducerInNiprResult")]
    [Bindable("searchProducerInNiprFault")]
    public function get fullNameFromNipr():String {
        return _fullName;
    }

    [Bindable("searchProducerInNiprResult")]
    [Bindable("searchProducerInNiprFault")]
    public function get dateOfBirthFromNipr():String {
        return _dateOfBirth;
    }

    [Bindable("searchProducerInNiprResult")]
    [Bindable("searchProducerInNiprFault")]
    public function get fullNameFromNiprCorrected():String {
        var nameParts:Array = _fullName.split(", ");
        nameParts.push(nameParts[0]);
        nameParts.shift();

        return nameParts.join(" ");
    }

    private var _ssnVerified:String = "N";

    [Bindable("searchProducerInNiprResult")]
    [Bindable("searchProducerInNiprFault")]
    [Bindable("checkProducerSSNInNiprResult")]
    [Bindable("checkProducerSSNInNiprFault")]
    public function get ssnVerified():String {
        return _ssnVerified;
    }

    private var _isCarrierResponsibleForPayment:Boolean;

	[Bindable("paymentResponsibilityResult")]
	[Bindable("paymentResponsibilityFault")]
	public function get isCarrierResponsibleForPayment():Boolean {
		return _isCarrierResponsibleForPayment;
	}


    private var _existingContracts:ArrayCollection;

	[Bindable("contractsCheckResult")]
	[Bindable("contractsCheckFault")]
	public function get existingContracts():ArrayCollection {
		return _existingContracts;
	}

    private var _updateProducerInfoTransactionCost:Number;

	[Bindable("updateProducerInfoTransactionCostResult")]
	[Bindable("updateProducerInfoTransactionCostFault")]
	public function get updateProducerInfoTransactionCost():Number {
		return _updateProducerInfoTransactionCost;
	}


    //------------------------------------------------------------------
    //
    //  Service Handlers
    //
    //------------------------------------------------------------------

    public function getNiprFirstName():String {
        var result:String = "";
        var fullName:String = fullNameFromNiprCorrected;
        if (fullName) {
            var nameParts:Array = fullName.split(" ");
            result = nameParts[0];
        }
        return result;
    }

    public function getNiprLastName():String {
        var result:String = "";
        var fullName:String = fullNameFromNipr;
        if (fullName) {
            var nameParts:Array = fullName.split(",");
            result = nameParts.length > 0 ? nameParts[0] : "";
        }
        return result;
    }

    public function activate():void {
        activateServices();
    }

    public function deactivate():void {
        deactivateServices();
    }

    public function searchProducerInNiprBySSN(ssn:String, lastName:String):void {
        setProgress(true, "Waiting for NIPR details...");
        _ssn = ssn;
        _niprHitlistService.loadInfoBySSN(ssn, lastName);
        _reasonToLoadInfoFromNipr = SEARCH_PRODUCER_REASON;
    }

    public function checkProducerSNNInNipr(ssn:String):void {
        setProgress(true, "Waiting for NIPR details...");
        _ssn = ssn;
        _niprHitlistService.loadInfoBySSN(ssn, getNiprLastName());
        _reasonToLoadInfoFromNipr = CHECK_PRODUCER_SNN_REASON;
    }

    public function searchProducerInNiprByLicense(licenseNo:String, stateId:String):void {
        setProgress(true, "Waiting for NIPR details...");
        _licenseNoValue = licenseNo;
        _niprHitlistService.loadInfoByLicense(licenseNo, stateId);
        _reasonToLoadInfoFromNipr = SEARCH_PRODUCER_REASON;
    }

    public function checkIsProducerExist(npn:String):void {
        setProgress(true, "Checking Your Identity...");
        _npnService.getProducerByNpn(npn);

        if (npn.indexOf("Y{")>=0)
            _npn = npn;                 // saving fake NPN for captive producer
    }

    public function checkIsProducerLinkedToBga(gaId:Number, producerId:Number):void {
        setProgress(true, "Checking Your Identity...");
        _ga2producerService.findBga2Producer(gaId, producerId);
    }

    public function checkIsPDBReport(npn:String):void {
        setProgress(true, "Checking Your Identity...");
        _pdbService.getPdbByNpn(npn);
    }

    public function loadPdbReport(gaId:Number, npn:String, ssn:String):void {
        setProgress(true, "Loading PDB Report...");
        _loadPdbReportService.getOrCreatePdbByNpn_LongCall(gaId, npn, ssn);
    }

    public function loadAmsPersonalInfo(ssn:String, gaDTO:ModelBgaDTO):void {
        if (gaDTO && gaDTO.amsDTO) {
            var amsService:AMS = AMSFactory.instance.createAMS(gaDTO.amsDTO);
            if (amsService) {
                setProgress(true, "Load AMS Personal Info...");
                amsService.getProducer(ssn, null, gaDTO.id, ams_getProducerResultHandler, ams_getProducerFaultHandler);
            }
        }
    }

    public function loadPaymentResponsibility(gaId:Number):void {
	    setProgress(true, "Loading Payment Responsibility...");

        _paymentResponsibilityService.checkIfCarrierPayment(gaId, "Agency");
    }

    public function loadContractsInformation(gaId:Number, uplineId: Number, token: String):void {
	    setProgress(true, "Checking Your Contracts...");

        RemotingUtils.invoke("com.farata.InvitationService", "getExistingContractsForInvite", _producerId, gaId, uplineId, token)
                .then(getExistingContractsForInvite_result, getExistingContractsForInvite_fault);
    }

    public function proceedWithAppointmentRequest(gaId:Number, token: String):void {
        setProgress(true, "Creating contract request...");

        RemotingUtils.invoke("com.farata.InvitationService", "proceedWithAppointmentRequest", _producerId, gaId, token)
                .then(proceedWithAppointmentRequest_resultHandler, proceedWithAppointmentRequest_faultHandler);
    }

    public function loadUpdateProducerInfoTransactionCost(gaId:Number, carrierId:Number):void {
        setProgress(true, "Loading Payment Details...");

        _transactionCostService.getUpdateProducerInfoTransactionCost(gaId, carrierId, "add_producer");
    }

    public function addProducer(loginSessionDTO:LoginSessionDTO, gaId: Number, deferActivation:Boolean):void {
        setProgress(true, "Adding Producer...");

        var clone:LoginSessionDTO = LoginSessionDTO(loginSessionDTO.clone());
        var defaultPassword:String = new Date().time.toString();
        clone.passwordValue = StringUtils.getHash(defaultPassword);

        if (SecurityContext.instance.gaDTO && SecurityContext.instance.gaDTO.notifyOnProducerCreation == "Y" && (SecurityContext.instance.isSubAgencyWorker || !SecurityContext.instance.isBgaAdmin)) {
            // #7582: we should not overwrite loginSession.cc and .bcc with any values
            // values from ga.addProducerConfig are no longer used here

            var ccList:Array = loginSessionDTO.ccList;
            if (!SecurityContext.instance.isSubAgencyWorker && SecurityContext.instance.gaDTO.useAffiliationInfo == "Y") {
                var subEmail:String = SecurityContext.instance.gaDTO.getAffiliationInfo(loginSessionDTO.branchValue).email;
                if (subEmail && ccList.indexOf(subEmail) == -1)
                    ccList.push(subEmail);
            }
            clone.cc = ccList.join(",");

            var bccList:Array = loginSessionDTO.bccList;
            if (SecurityContext.instance.gaDTO.email && bccList.indexOf(SecurityContext.instance.gaDTO.email) == -1)
                bccList.push(SecurityContext.instance.gaDTO.email);
            clone.bcc = bccList.join(",");
        }
        if (isAddingImportedProducer) {
            _producerService.addImportedProducerWorkflow_LongCall(clone, gaId, true, amsPersonalInfo, deferActivation, ssnVerified, producerId);
        } else {
            _producerService.addProducerWorkflow_LongCall(clone, gaId, true, amsPersonalInfo, deferActivation, ssnVerified);
        }
    }

    public function checkEmail(email:String, lastName:String, gaId:int):AsyncToken {
        var producerService:RemoteObject = new RemoteObject("com.farata.ProducerServiceExtender");
        return producerService.addProducerWorkflowCheckEmail(email, lastName, gaId);
    }

    public function setProgress(progress:Boolean, message:String = ""):void {
        if (_inProgress != progress) {
            _inProgress = progress;
            _progressMessage = message;
            dispatchEvent(new Event("inProgressChange"));
        }
    }

    protected function ams_getProducerResultHandler(result:ModelProducerDTO, token:Object = null):void {
        setProgress(false);
        _amsPersonalInfo = result;
        dispatchEvent(new DynamicEvent(LOAD_AMS_PERSONAL_INFO_RESULT));
    }

    //------------------------------------------------------------------
    //
    //  Methods
    //
    //------------------------------------------------------------------

    protected function ams_getProducerFaultHandler(event:FaultEvent, token:Object = null):void {
        setProgress(false);
        _amsPersonalInfo = null;
        dispatchEvent(new DynamicEvent(LOAD_AMS_PERSONAL_INFO_FAULT));
        LOG.error('' + event.fault);
    }

    protected function activateServices():void {
        _niprHitlistService = new NiprHitListService();
        _niprHitlistService.addEventListener(NiprHitListService.LOAD_INFO_RESULT, niprHitListService_loadInfoResultHandler);
        _niprHitlistService.addEventListener(NiprHitListService.LOAD_INFO_FAULT, niprHitListService_loadInfoFaultHandler);

        _npnService = new RemoteObject("com.farata.ProducerService");
        _npnService.addEventListener(ResultEvent.RESULT, getProducerByNpn_resultHandler);
        _npnService.addEventListener(FaultEvent.FAULT, getProducerByNpn_faultHandler);

        _ga2producerService = new RemoteObject("com.farata.BgaService");
        _ga2producerService.addEventListener(ResultEvent.RESULT, getBgaToProducer_resultHandler);
        _ga2producerService.addEventListener(FaultEvent.FAULT, getBgaToProducer_faultHandler);

        _pdbService = new RemoteObject("com.farata.ProducerService");
        _pdbService.addEventListener(ResultEvent.RESULT, getPdbByNpn_resultHandler);
        _pdbService.addEventListener(FaultEvent.FAULT, getPdbByNpn_faultHandler);

        _loadPdbReportService = new RemoteObject("com.farata.ProducerServiceExtender");
        _loadPdbReportService.addEventListener(ResultEvent.RESULT, loadPdbReport_resultHandler);
        _loadPdbReportService.addEventListener(FaultEvent.FAULT, loadPdbReport_faultHandler);

        _producerService = new RemoteObject("com.farata.ProducerServiceExtender");
        _producerService.addEventListener(ResultEvent.RESULT, addProducer_resultHandler);
        _producerService.addEventListener(FaultEvent.FAULT, addProducer_faultHandler);

	    _paymentResponsibilityService = new RemoteObject("com.farata.ProducerServiceExtender");
	    _paymentResponsibilityService.addEventListener(ResultEvent.RESULT, paymentResponsibilityService_resultHandler);
	    _paymentResponsibilityService.addEventListener(FaultEvent.FAULT, paymentResponsibilityService_faultHandler);

	    _transactionCostService = new RemoteObject("com.farata.ProducerServiceExtender");
	    _transactionCostService.addEventListener(ResultEvent.RESULT, transactionCostService_resultHandler);
	    _transactionCostService.addEventListener(FaultEvent.FAULT, transactionCostService_faultHandler);
    }

    protected function deactivateServices():void {
        _niprHitlistService.removeEventListener(NiprHitListService.LOAD_INFO_RESULT, niprHitListService_loadInfoResultHandler);
        _niprHitlistService.removeEventListener(NiprHitListService.LOAD_INFO_FAULT, niprHitListService_loadInfoFaultHandler);
        _niprHitlistService = null;

        _npnService.removeEventListener(ResultEvent.RESULT, getProducerByNpn_resultHandler);
        _npnService.removeEventListener(FaultEvent.FAULT, getProducerByNpn_faultHandler);
        _npnService = null;

        _ga2producerService.removeEventListener(ResultEvent.RESULT, getBgaToProducer_resultHandler);
        _ga2producerService.removeEventListener(FaultEvent.FAULT, getBgaToProducer_faultHandler);
        _ga2producerService = null;

        _pdbService.removeEventListener(ResultEvent.RESULT, getPdbByNpn_resultHandler);
        _pdbService.removeEventListener(FaultEvent.FAULT, getPdbByNpn_faultHandler);
        _pdbService = null;

        _loadPdbReportService.removeEventListener(ResultEvent.RESULT, loadPdbReport_resultHandler);
        _loadPdbReportService.removeEventListener(FaultEvent.FAULT, loadPdbReport_faultHandler);
        _loadPdbReportService = null;

        _producerService = new RemoteObject("com.farata.ProducerServiceExtender");
        _producerService.removeEventListener(ResultEvent.RESULT, addProducer_resultHandler);
        _producerService.removeEventListener(FaultEvent.FAULT, addProducer_faultHandler);

	    _paymentResponsibilityService.removeEventListener(ResultEvent.RESULT, paymentResponsibilityService_resultHandler);
	    _paymentResponsibilityService.removeEventListener(FaultEvent.FAULT, paymentResponsibilityService_faultHandler);
	    _paymentResponsibilityService = null;

	    _transactionCostService.removeEventListener(ResultEvent.RESULT, transactionCostService_resultHandler);
	    _transactionCostService.removeEventListener(FaultEvent.FAULT, transactionCostService_faultHandler);
	    _transactionCostService = null;
    }

    protected function niprHitListService_loadInfoResultHandler(event:DynamicEvent):void {
        setProgress(false);
        if (event.data.entityType == "individual") {
            _ssnVerified = event.data.report_type == NiprHitListService.SSN_REPORT_TYPE ? "Y" : _ssnVerified;
            if (SEARCH_PRODUCER_REASON == _reasonToLoadInfoFromNipr) {
                _npn = event.data.npn;
                _fullName = event.data.name;
                _dateOfBirth = event.data.dateOfBirth;
                dispatchEvent(new DynamicEvent(SEARCH_PRODUCER_IN_NIPR_RESULT));
            } else if (CHECK_PRODUCER_SNN_REASON == _reasonToLoadInfoFromNipr) {
                _ssnVerified = _npn != event.data.npn ? "N" : _ssnVerified;
                dispatchEvent(new DynamicEvent(CHECK_PRODUCER_SSN_IN_NIPR_RESULT));
            }
        } else {
            _npn = "";
            _fullName = "";
            _dateOfBirth = "";
            var faultEvent:DynamicEvent = new DynamicEvent(SEARCH_PRODUCER_IN_NIPR_FAULT);
            faultEvent.recordTypeMismatch = true;
            faultEvent.hitListFinishWorkWith = ResultEvent.RESULT;
            dispatchEvent(faultEvent);
        }
        _reasonToLoadInfoFromNipr = "";

    }

    protected function niprHitListService_loadInfoFaultHandler(event:DynamicEvent):void {
        setProgress(false);
        _ssnVerified = event.data && event.data.report_type == NiprHitListService.SSN_REPORT_TYPE ? "N" : _ssnVerified;

        var faultEvent:DynamicEvent = null;
        if (SEARCH_PRODUCER_REASON == _reasonToLoadInfoFromNipr) {
            _npn = "";
            _fullName = "";
            _dateOfBirth = "";
            faultEvent = new DynamicEvent(SEARCH_PRODUCER_IN_NIPR_FAULT);
        } else if (CHECK_PRODUCER_SNN_REASON == _reasonToLoadInfoFromNipr) {
            faultEvent = new DynamicEvent(CHECK_PRODUCER_SSN_IN_NIPR_FAULT);
        }

        if (faultEvent) {
            faultEvent.hitListFinishWorkWith = event.data.hitListEventType;
            dispatchEvent(faultEvent);
        }

        _reasonToLoadInfoFromNipr = "";
        LOG.error('NiprHitListService load Info Fault');
    }

    protected function getProducerByNpn_resultHandler(event:ResultEvent):void {
        setProgress(false);
        var data: ProducerVeryShortInfoDTO = ArrayCollection(event.result).length > 0
                ? ArrayCollection(event.result).getItemAt(0) as ProducerVeryShortInfoDTO
                : null;

        _isProducerExist = (data != null && data.producerId && !isNaN(data.producerId) && data.producerId > 0);
        _producerId = data != null ? data.producerId : NaN;
        dispatchEvent(new DynamicEvent(IS_PRODUCER_EXIST_RESULT, false, false, data));

        if (_npn.indexOf("Y{")>=0) {
            var token:AsyncToken = new RemoteObject("com.farata.ProducerService").getModelProducerByNPN(_npn);
            token.addResponder(new AsyncResponder(onGetCaptiveProducer, onFault));
        }
    }

    protected function onGetCaptiveProducer(event:ResultEvent, token:AsyncToken = null):void {
        _captiveProducerModel = ArrayCollection(event.result).length > 0 ? ArrayCollection(event.result).getItemAt(0) as ModelProducerDTO : null;
        _fullName = _captiveProducerModel != null ? _captiveProducerModel.fullName2 : "";
    }

    private function onFault(event:FaultEvent, token:AsyncToken = null):void {
        ApplicationErrorManager.addError(new ApplicationError(event, event.fault ? event.fault.faultString : "Error"));
    }


    protected function getProducerByNpn_faultHandler(event:FaultEvent):void {
        setProgress(false);
        _isProducerExist = false;
        dispatchEvent(new DynamicEvent(IS_PRODUCER_EXIST_FAULT));
        LOG.error('' + event.fault);
    }

    protected function getBgaToProducer_resultHandler(event:ResultEvent):void {
        setProgress(false);
        var data:GA2ProducerDTO = ArrayCollection(event.result).length > 0
                ? ArrayCollection(event.result).getItemAt(0) as GA2ProducerDTO
                : null;
        LOG.info("getBgaToProducer result: " + (data != null ? data.toString() : "null") + " - ");
        _isProducerLinkedToBga = (data != null && data.id && !isNaN(data.id) && data.id > 0);
        dispatchEvent(new DynamicEvent(IS_PRODUCER_LINKED_TO_BGA_RESULT, false, false, data));
    }

    protected function getBgaToProducer_faultHandler(event:FaultEvent):void {
        setProgress(false);
        _isProducerLinkedToBga = false;
        dispatchEvent(new DynamicEvent(IS_PRODUCER_LINKED_TO_BGA_FAULT));
        LOG.error('' + event.fault);
    }

    protected function getPdbByNpn_resultHandler(event:ResultEvent):void {
        setProgress(false);
        var data:Object = {pdbId: event.result.outParameter1, authorizedByGAs: event.result.outParameter2};
        _hasPdbReport = (data.pdbId && !isNaN(data.pdbId) && data.pdbId > 0);
        dispatchEvent(new DynamicEvent(HAS_PDB_REPORT_RESULT, false, false, data));
    }

    protected function getPdbByNpn_faultHandler(event:FaultEvent):void {
        setProgress(false);
        _hasPdbReport = false;
        dispatchEvent(new DynamicEvent(HAS_PDB_REPORT_FAULT));
        LOG.error('' + event.fault);
    }

    protected function loadPdbReport_resultHandler(event:ResultEvent):void {
        setProgress(false);
        dispatchEvent(new DynamicEvent(LOAD_PDB_REPORT_RESULT));
    }

    protected function loadPdbReport_faultHandler(event:FaultEvent):void {
        setProgress(false);
        dispatchEvent(new DynamicEvent(LOAD_PDB_REPORT_FAULT));
        LOG.error('' + event.fault);
    }

    protected function addProducer_resultHandler(event:ResultEvent):void {
        setProgress(false);
        var result:Object = event.result;
        _userInfo = result.userInfo as UserInfoDTO;
        _producerDTO = result.producerDTO as ModelProducerDTO;
        if (_userInfo && _producerDTO && result.success) {
            dispatchEvent(new DynamicEvent(ADD_PRODUCER_RESULT));
        } else {
            var errorData:Object = {
                errorId: result.errorId,
                errorMessage: result.errorMessage,
                showLogin: result.showLogin
            };
            dispatchEvent(new DynamicEvent(ADD_PRODUCER_FAULT, false, false, errorData));
        }
    }

    protected function addProducer_faultHandler(event:FaultEvent):void {
        setProgress(false);
        _userInfo = null;
        _producerDTO = null;
        ApplicationErrorManager.showApplicationError(event, "Producer creation error\n" + event.fault.faultString);
        dispatchEvent(new DynamicEvent(ADD_PRODUCER_FAULT));
        LOG.error('' + event.fault);
    }

	private function paymentResponsibilityService_resultHandler(event:ResultEvent):void {
        setProgress(false);

        if (event && event.result is Boolean)
            _isCarrierResponsibleForPayment = event.result as Boolean;

		dispatchEvent(new DynamicEvent(PAYMENT_RESPONSIBILITY_RESULT));
	}

	private function paymentResponsibilityService_faultHandler(event:FaultEvent):void {
		setProgress(false);
		_isCarrierResponsibleForPayment = false;

		ApplicationErrorManager.showApplicationError(event, "Failed to retrieve payment responsibility\n" + event.fault.faultString);
		dispatchEvent(new DynamicEvent(PAYMENT_RESPONSIBILITY_FAULT));
		LOG.error('' + event.fault);
	}

	private function getExistingContractsForInvite_result(result:*):void {
        setProgress(false);

        _existingContracts = result as ArrayCollection;

		dispatchEvent(new DynamicEvent(CONTRACTS_CHECK_RESULT));
	}

	private function getExistingContractsForInvite_fault(event:FaultEvent):void {
        setProgress(false);

        _existingContracts = new ArrayCollection();

		ApplicationErrorManager.showApplicationError(event, "Failed to retrieve contracts information\n" + event.fault.faultString);
		dispatchEvent(new DynamicEvent(CONTRACTS_CHECK_FAULT));
		LOG.error('' + event.fault);
	}

	private function proceedWithAppointmentRequest_resultHandler(result:*):void {
        setProgress(false);

        var pendingAppointments: ArrayCollection = result as ArrayCollection;

		dispatchEvent(new DynamicEvent(PROCEED_WITH_APP_REQ_RESULT, false, false, pendingAppointments));
	}

	private function proceedWithAppointmentRequest_faultHandler(event:FaultEvent):void {
        setProgress(false);

        _existingContracts = new ArrayCollection();

        ApplicationErrorManager.showApplicationError(event, "Failed to create appointment requests\n" + event.fault.faultString);
        dispatchEvent(new DynamicEvent(PROCEED_WITH_APP_REQ_FAULT));
    }

	private function transactionCostService_resultHandler(event:ResultEvent):void {
		setProgress(false);

		if (event && !isNaN(Number(event.result)))
			_updateProducerInfoTransactionCost = Number(event.result);

		dispatchEvent(new DynamicEvent(UPDATE_PRODUCER_INFO_TRANSACTION_COST_RESULT));
	}

	private function transactionCostService_faultHandler(event:FaultEvent):void {
		setProgress(false);
		_updateProducerInfoTransactionCost = Number.NaN;

		ApplicationErrorManager.showApplicationError(event, "Failed to retrieve payment details \n" + event.fault.faultString);
		dispatchEvent(new DynamicEvent(UPDATE_PRODUCER_INFO_TRANSACTION_COST_FAULT));

		LOG.error('' + event.fault);
	}
}
}
