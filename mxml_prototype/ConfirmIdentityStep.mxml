<?xml version="1.0"?>
<steps:AddProducerStepBase xmlns:fx="http://ns.adobe.com/mxml/2009"
                           xmlns:steps="view.addProducer2.steps.*"
                           xmlns:s="library://ns.adobe.com/flex/spark"
                           xmlns:fs="library://ns.cleartoolkit.com/flex/clear"
                           xmlns:lib="http://www.surancebay.com/2009/components"
                           xmlns:supportclasses="view.addProducer2.supportClasses.*"
                           showNextButton="true"
                           showCancelButton="true"
                           nextButtonEnabled="{isInputDataValid}">
    <fx:Declarations>
        <fs:ValidationRule id="ssnValidator"
                           enabled="{model.identificationByLicense}" required="{model.identificationByLicense}"
                           requiredFieldError="Please enter social security number"
                           source="{ssnInput}" property="text"
                           validationFunction="ssnValidationFunction"/>
        <fs:ValidationRule id="dateValidator"
                           errorMessage="date in MM/DD/YYYY format is required"
                           requiredFieldError="date in MM/DD/YYYY format is required"
                           required="true"
                           source="{dobDateChooser}" property="selectedDate" triggerEvent="dateChange"
                           validationFunction="{ModelProducerValidators.date}"/>
        <fs:RemoteObject id="checkSSNRemoteObject"
                         destination="com.farata.ProducerService"
                         result="checkSSNRemoteObject_resultHandler(event)"
                         fault="checkSSNRemoteObject_faultHandler(event)"/>
    </fx:Declarations>

    <steps:stepContent>
        <s:VGroup width="100%" gap="15" paddingTop="15" paddingBottom="10">
            <s:Label text="Confirming {isProducerFlow ? 'your' : 'producer\'s'} identity" styleName="titleDisplay" paddingLeft="10"/>

            <s:SkinnableContainer width="100%" backgroundColor="#dfe2e7">
                <s:layout>
                    <s:VerticalLayout paddingBottom="14" paddingRight="14" paddingLeft="14" paddingTop="14"/>
                </s:layout>

                <s:RichText width="100%" styleName="infoDisplay">
                    <s:content>{isProducerFlow ? controller.fullNameFromNiprCorrected + ', we still need to confirm your' : 'We still need to confirm ' + controller.fullNameFromNiprCorrected +'\'s'} identity
                        before you can proceed with the account setup.
                    </s:content>
                </s:RichText>
            </s:SkinnableContainer>

            <s:RichText width="100%" styleName="infoDisplayMini" paddingLeft="10">
                <s:content>Please type in {isProducerFlow ? "your" : "producer's"} date of birth, press Enter key {model.identificationByLicense ? 'and type in ' + (isProducerFlow ? 'your' : 'producer\'s') + ' SSN' : ''} and then press <s:span fontWeight="bold">Next</s:span>.
                </s:content>
            </s:RichText>

            <s:Line width="100%">
                <s:stroke>
                    <s:SolidColorStroke weight="1" color="#000000" />
                </s:stroke>
            </s:Line>

            <supportclasses:UnableToConnectWarningView visible="{unableCompleteProcessMessage}"
                                                       includeInLayout="{unableCompleteProcessMessage}"
                                                       width="100%"
                                                       text="{unableCompleteProcessMessage}"/>

            <s:HGroup gap="50" styleName="infoDisplayMini" paddingLeft="10">
                <s:VGroup>
                    <s:Label text="Date of birth (MM/DD/YYYY)" fontWeight="bold"/>
                    <s:HGroup verticalAlign="middle">
                        <lib:DateChooserLine id="dobDateChooser"
                                             usePrediction="false"
                                             maxDate="{DateUtils.getDob(18)}"
                                             minDate="{DateUtils.getDob(99)}"
                                             selectedDate="{DateUtils.stringToDate(model.dob)}"
                                             dateChange="dobDateChooser_dateChangeHandler(event)"
                                             creationComplete="dobDateChooser_creationCompleteHandler(event)"
                                             dontShowCalendar="true" showCurrentButton="false"
                                             commitDateOnInput="true"
                                             width="{DPI.width(12)}"/>
                        <fs:CalloutButton id="dobHelpButton" glyph="{IG.HELP}" calloutContent="{[contactYourBgaRichText]}"/>
                    </s:HGroup>
                </s:VGroup>

                <s:VGroup visible="{model.identificationByLicense}" enabled="{!checkingSsn}">
                    <s:Label text="SSN" fontWeight="bold"/>

                    <s:HGroup verticalAlign="middle">
                        <fs:MaskedTextInput id="ssnInput" text="@{model.ssnValue}"
                                            width="{DPI.width(20)}"
                                            visible="{model.identificationByLicense}"
                                            tabIndex="{secondControlTabIndex}"
                                            inputMask="ssn" softKeyboardType="number"
                                            prompt="#########"
                                            change="ssnInput_changeHandler(event)"/>
                        <fs:CalloutButton id="ssnHelpButton" glyph="{IG.HELP}" calloutContent="{[contactYourBgaRichText]}"/>
                        <fs:ProgressIndicator visible="{checkingSsn}"/>
                    </s:HGroup>
                </s:VGroup>
            </s:HGroup>

            <s:Line width="100%">
                <s:stroke>
                    <s:SolidColorStroke weight="1" color="#000000" />
                </s:stroke>
            </s:Line>
        </s:VGroup>
    </steps:stepContent>

    <fx:Script>
        <![CDATA[
        import clear.events.DynamicEvent;

        import com.farata.controls.MessageBox;
        import com.farata.core.DPI;
        import com.farata.dashboard.security.SecurityContext;
        import com.farata.dto.GA2ProducerDTO;
        import com.farata.dto.ProducerVeryShortInfoDTO;
        import com.farata.utils.DateUtils;
        import com.farata.validators.GeneralValidators;
        import com.farata.validators.ModelProducerValidators;
        import com.farata.webservice.NiprHitListService;

        import graphics.IG;

        import mx.collections.ArrayCollection;
        import mx.events.FlexEvent;
        import mx.events.ValidationResultEvent;
        import mx.logging.ILogger;
        import mx.logging.Log;
        import mx.rpc.events.FaultEvent;
        import mx.rpc.events.ResultEvent;
        import mx.utils.ObjectUtil;

        import spark.events.TextOperationEvent;
        import spark.events.ViewNavigatorEvent;

        import view.addProducer2.AddProducerWorkflow;
        import view.addProducer2.supportClasses.AddProducerController;

        //------------------------------------------------------------------
        //
        //  Class Constants
        //
        //------------------------------------------------------------------

        private static const LOG:ILogger = Log.getLogger("ConfirmIdentityStep");

        //------------------------------------------------------------------
        //
        //  Variables
        //
        //------------------------------------------------------------------

        [Bindable]
        public var unableCompleteProcessMessage:String = "";

        [Bindable]
        protected var checkingSsn:Boolean;

        private var _ssnExist:Boolean;

        private var enterPressedCount:int = 0;
        //------------------------------------------------------------------
        //
        //  Properties
        //
        //------------------------------------------------------------------

        [Bindable("dobInputChange")]
        public function get isDOBValid():Boolean {
            var result:ValidationResultEvent = dateValidator.validate();
            return result && result.type == ValidationResultEvent.VALID;
        }

        [Bindable("checkSSNChange")]
        [Bindable("ssnInputChange")]
        protected function get isSsnValid():Boolean {
            var result:ValidationResultEvent = ssnValidator.validate();
            return result && result.type == ValidationResultEvent.VALID;
        }

        [Bindable("checkSSNChange")]
        [Bindable("ssnInputChange")]
        [Bindable("dobInputChange")]
        [Bindable("creationComplete")]
        protected function get isInputDataValid():Boolean {
            var isValid:Boolean = isDOBValid;
            if (model.identificationByLicense) {
                isValid = isSsnValid && isValid;
            }
            return isValid;
        }

        //------------------------------------------------------------------
        //
        //  This Handlers
        //
        //------------------------------------------------------------------

        override protected function this_keyUpHandler(event:KeyboardEvent):void {
            switch (event.keyCode) {
                case Keyboard.ENTER:
                    enterPressedCount++;
                    if (enterPressedCount % 2 == 0)
                        super.this_keyUpHandler(event);
                    else {
                        if (ssnInput.visible)
                            ssnInput.setFocus();
                        else if (nextButton.enabled)
                            nextButton.setFocus();
                    }
                    break;

                default:
                    super.this_keyUpHandler(event);
                    break;
            }
        }

        //------------------------------------------------------------------
        //
        //  Service Handlers
        //
        //------------------------------------------------------------------

        protected function checkProducerSSN_resultHandler(event:DynamicEvent):void {
            removeCheckProducerSSNEventListeners();
            producerSNNCheckedHandler();
        }

        protected function checkProducerSSN_faultHandler(event:DynamicEvent):void {
            removeCheckProducerSSNEventListeners();
            if (event.hitListFinishWorkWith == ResultEvent.RESULT) {
                producerSNNCheckedHandler();
            } else {
                unableCompleteProcessMessage = NiprHitListService.SERVICES_UNAVAILABLE_ERROR;
                stopAutoAddProcess();
            }
        }

        protected function isProducerExistWithNPN_resultHandler(event:DynamicEvent):void {
            removeIsProducerExistEventListeners();
            if (controller.isProducerExist) {
                controller.checkIsProducerLinkedToBga(securityContext.proposedGaId, ProducerVeryShortInfoDTO(event.data).producerId);
                addIsProducerLinkedEventListeners();
            } else {
                controller.checkIsPDBReport(model.npnValue);
                addHasPDBReportEventListeners();
            }
        }

        protected function isProducerExistWithNPN_faultHandler(event:DynamicEvent):void {
            removeIsProducerExistEventListeners();
            unableCompleteProcessMessage = AddProducerController.CAN_NOT_CHECK_YOUR_IDENTITY_ERROR;
            stopAutoAddProcess();
        }

        protected function isProducerLinkedToBga_resultHandler(event:DynamicEvent):void {
            removeIsProducerLinkedEventListeners();
            if (controller.isProducerLinkedToBga) {
                var securityContext:SecurityContext = SecurityContext.instance;

                var branchCode:String = GA2ProducerDTO(event.data).branchCode;

                if (securityContext.isSubAgencyWorker && securityContext.userAgencyBranches.indexOf(branchCode) == -1) {
                    MessageBox.show(this,
                            controller.fullNameFromNiprCorrected + " already exists in your agency, but has different affiliation. Please contact your office manager for details.", "Warning",
                            MessageBox.OK);
                } else {
                    // #13209 Invite Downline logic when the downline is already contracted with different Direct Upline.
                    if (model.uplineId > 0) {
                        goToStep(AddProducerWorkflow.DIRECT_UPLINE_STEP);
                    } else {
                        goToStep(AddProducerWorkflow.ACCOUNT_EXIST_STEP);
                    }
                }
            } else {
                controller.checkIsPDBReport(model.npnValue);
                addHasPDBReportEventListeners();
            }
        }

        protected function isProducerLinkedToBga_faultHandler(event:DynamicEvent):void {
            removeIsProducerLinkedEventListeners();
            unableCompleteProcessMessage = AddProducerController.CAN_NOT_CHECK_YOUR_IDENTITY_ERROR;
            stopAutoAddProcess();
        }

        protected function hasPDBReport_resultHandler(event:DynamicEvent):void {
            removeHasPDBReportEventListeners();
            var isPdbAuthorized:Boolean;
            if (controller.hasPdbReport) {
                isPdbAuthorized = isGAAuthorizedPdb(securityContext.proposedGaId, event.data.authorizedByGAs);
            }
            //Adding imported producer that not linked to this ga
            controller.isAddingImportedProducer = controller.isProducerExist && !controller.hasPdbReport;

            if (isPdbAuthorized) {
                if (skipAddContactDataStep) {
                    goToStep(AddProducerWorkflow.REVIEW_AND_CONFIRMATION_STEP);
                } else {
                    goToStep(AddProducerWorkflow.ADD_CONTACT_DATA_STEP);
                }
            } else {
                goToStep(AddProducerWorkflow.CONFIRM_NIPR_ACCESS_STEP);
            }
        }

        protected function hasPDBReport_faultHandler(event:DynamicEvent):void {
            removeHasPDBReportEventListeners();
            unableCompleteProcessMessage = AddProducerController.CAN_NOT_CHECK_YOUR_IDENTITY_ERROR;
            stopAutoAddProcess();
        }

        protected function checkSSNRemoteObject_resultHandler(event:ResultEvent):void {
            checkingSsn = false;
            var result:ArrayCollection = event.result is ArrayCollection ? event.result as ArrayCollection : null;
            _ssnExist = result && result.length > 0;
            dispatchEvent(new Event("checkSSNChange"));
            if (autoAddInProgress) {
                goToNextStepHandler();
            }
        }

        protected function checkSSNRemoteObject_faultHandler(event:FaultEvent):void {
            checkingSsn = false;
            _ssnExist = false;
            dispatchEvent(new Event("checkSSNChange"));
            stopAutoAddProcess();
        }

        //------------------------------------------------------------------
        //
        //  UI Handlers
        //
        //------------------------------------------------------------------

        protected function dobDateChooser_dateChangeHandler(event:Event):void {
            dispatchEvent(new Event("dobInputChange"));
        }

        protected function ssnInput_changeHandler(event:TextOperationEvent):void {
            ssnChangeHandler(ssnInput.text);
        }

        protected function ssnChangeHandler(ssn:String):void {
            if (GeneralValidators.ssn(ssn)) {
                checkingSsn = true;
                checkSSNRemoteObject.checkSSNForIndividual(ssn, model.npnValue);
            }
            dispatchEvent(new Event("ssnInputChange"));
        }

        protected function dobDateChooser_creationCompleteHandler(event:FlexEvent):void {                                                                                                   F
            if (dobDateChooser.tiDate) {
                dobDateChooser.tiDate.tabIndex = firstControlTabIndex;
            }
        }

        //------------------------------------------------------------------
        //
        //  Methods
        //
        //------------------------------------------------------------------

        public static function isDOBMatch(niprDOB:Date, selectedDOB:Date):Boolean {
            var result:Boolean = false;
            if (niprDOB && selectedDOB) {
                result = ObjectUtil.dateCompare(DateUtils.pureDate(niprDOB), DateUtils.pureDate(selectedDOB)) == 0;
            }
            return result;
        }

        protected function ssnValidationFunction(obj:Object, name:String = "SSN"):String {
            var error:String = GeneralValidators.ssnErrors(obj, name);
            if (!error && !checkingSsn && _ssnExist) {
                error = "SSN number must be unique. Please enter new one."
            }
            return error;
        }

        protected function producerSNNCheckedHandler():void {
            if (controller.ssnVerified == "Y") {
                controller.checkIsProducerExist(model.npnValue);
                addIsProducerExistEventListeners();
            } else {
                stopAutoAddProcess();
                MessageBox.show
                (
                        this,
                        "Entered SSN value haven't passed the verification via the NIPR Producer Database for " + controller.fullNameFromNipr + ". Are you sure you want to proceed with creating this account?",
                        "Warning",
                        MessageBox.YESNO | MessageBox.ICONWARNING,
                        function (code:int, param:* = null):void {
                            if (code == MessageBox.YES) {
                                controller.checkIsProducerExist(model.npnValue);
                                addIsProducerExistEventListeners();
                            }
                        }
                );
            }
        }

        protected function addCheckProducerSSNEventListeners():void {
            controller.addEventListener(AddProducerController.CHECK_PRODUCER_SSN_IN_NIPR_RESULT, checkProducerSSN_resultHandler);
            controller.addEventListener(AddProducerController.CHECK_PRODUCER_SSN_IN_NIPR_FAULT, checkProducerSSN_faultHandler);
        }

        protected function addIsProducerExistEventListeners():void {
            controller.addEventListener(AddProducerController.IS_PRODUCER_EXIST_RESULT, isProducerExistWithNPN_resultHandler);
            controller.addEventListener(AddProducerController.IS_PRODUCER_EXIST_FAULT, isProducerExistWithNPN_faultHandler);
        }

        protected function removeIsProducerExistEventListeners():void {
            controller.removeEventListener(AddProducerController.IS_PRODUCER_EXIST_RESULT, isProducerExistWithNPN_resultHandler);
            controller.removeEventListener(AddProducerController.IS_PRODUCER_EXIST_FAULT, isProducerExistWithNPN_faultHandler);
        }

        protected function addIsProducerLinkedEventListeners():void {
            controller.addEventListener(AddProducerController.IS_PRODUCER_LINKED_TO_BGA_RESULT, isProducerLinkedToBga_resultHandler);
            controller.addEventListener(AddProducerController.IS_PRODUCER_LINKED_TO_BGA_FAULT, isProducerLinkedToBga_faultHandler);
        }

        protected function removeCheckProducerSSNEventListeners():void {
            controller.removeEventListener(AddProducerController.CHECK_PRODUCER_SSN_IN_NIPR_RESULT, checkProducerSSN_resultHandler);
            controller.removeEventListener(AddProducerController.CHECK_PRODUCER_SSN_IN_NIPR_FAULT, checkProducerSSN_faultHandler);
        }

        protected function removeIsProducerLinkedEventListeners():void {
            controller.removeEventListener(AddProducerController.IS_PRODUCER_LINKED_TO_BGA_RESULT, isProducerLinkedToBga_resultHandler);
            controller.removeEventListener(AddProducerController.IS_PRODUCER_LINKED_TO_BGA_FAULT, isProducerLinkedToBga_faultHandler);
        }

        protected function addHasPDBReportEventListeners():void {
            controller.addEventListener(AddProducerController.HAS_PDB_REPORT_RESULT, hasPDBReport_resultHandler);
            controller.addEventListener(AddProducerController.HAS_PDB_REPORT_FAULT, hasPDBReport_faultHandler);
        }

        protected function removeHasPDBReportEventListeners():void {
            controller.removeEventListener(AddProducerController.HAS_PDB_REPORT_RESULT, hasPDBReport_resultHandler);
            controller.removeEventListener(AddProducerController.HAS_PDB_REPORT_FAULT, hasPDBReport_faultHandler);
        }

        private static function isGAAuthorizedPdb(gaId:Number, authorizedGaIds:String):Boolean {
            var result:Boolean = false;
            if (!isNaN(gaId) && gaId > -1 && authorizedGaIds) {
                var gaIds:Array = authorizedGaIds.split("|");
                result = gaIds.indexOf(gaId.toString()) > -1;
            }
            return result;
        }

        //------------------------------------------------------------------
        //
        //  Overridden methods
        //
        //------------------------------------------------------------------

        override protected function goToNextStepHandler():void {
            if (isInputDataValid) {
                unableCompleteProcessMessage = "";
                if (!isDOBMatch(DateUtils.stringToDate(controller.dateOfBirthFromNipr), dobDateChooser.selectedDate)) {
                    LOG.debug(controller.dateOfBirthFromNipr);
                    unableCompleteProcessMessage = "The date you entered does not match the date of birth in the NIPR record. Please correct the value and try again.";
                }
                if (!unableCompleteProcessMessage) {
                    if (model.identificationByLicense) {
                        controller.checkProducerSNNInNipr(model.ssnValue);
                        addCheckProducerSSNEventListeners();
                    } else {
                        controller.checkIsProducerExist(model.npnValue);
                        addIsProducerExistEventListeners();
                    }
                } else {
                    dobDateChooser.setFocus();
                    stopAutoAddProcess();
                }
            } else {
                stopAutoAddProcess();
            }
        }

        override protected function startAutoAddProcess():void {
            super.startAutoAddProcess();
            if (model.identificationByLicense && !_ssnExist && model.ssnValue) {
                ssnChangeHandler(model.ssnValue);
            } else {
                goToNextStepHandler();
            }
        }

        override protected function get isAutoAddAllowed():Boolean {
            return addProducerWizard.isAutoAdd;
        }

        override protected function this_viewActivateHandler(event:ViewNavigatorEvent):void {
            super.this_viewActivateHandler(event);
            dobDateChooser.setFocus();
            enterPressedCount = 0;
            if (model.identificationByLicense)
                ssnInput.text = "";
        }

        override protected function updateEnableForControlsWithSSOValue():void {
            super.updateEnableForControlsWithSSOValue();
            if (dobDateChooser) {
                dobDateChooser.editable = canEditSSOParameter("dob");
                dobHelpButton.visible = dobHelpButton.includeInLayout = !dobDateChooser.editable;
            }
            if (ssnInput) {
                ssnInput.editable = canEditSSOParameter("ssnValue");
                ssnHelpButton.visible = ssnHelpButton.includeInLayout = !ssnInput.editable;
            }
        }
        ]]>
    </fx:Script>
</steps:AddProducerStepBase>
