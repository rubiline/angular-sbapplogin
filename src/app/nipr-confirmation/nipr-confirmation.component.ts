import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {CdkScrollable, ScrollDispatchModule} from '@angular/cdk/scrolling';

@Component({
  selector: 'app-nipr-confirmation',
  templateUrl: './nipr-confirmation.component.html',
  styleUrls: ['./nipr-confirmation.component.css']
})
export class NiprConfirmationComponent implements OnInit {

  carrier = false;
  agency = true;
  independent = false;

  scrolled = false;

  @ViewChild(CdkScrollable) scrollable: CdkScrollable;

  constructor() { }

  ngOnInit() {
  }
/*
  // scroll to end and then enable buttton
  private isScrolledToEnd(): boolean {
    let scrolled: boolean = true;
    if (scrolled && scroller.verticalScrollBar) {
      scrolled = scroller.verticalScrollBar.value >= (scroller.verticalScrollBar.maximum - 10);
    }
    return scrolled;
  }
  */

}
