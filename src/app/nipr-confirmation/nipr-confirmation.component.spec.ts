import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NiprConfirmationComponent } from './nipr-confirmation.component';

describe('NiprConfirmationComponent', () => {
  let component: NiprConfirmationComponent;
  let fixture: ComponentFixture<NiprConfirmationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NiprConfirmationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NiprConfirmationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
