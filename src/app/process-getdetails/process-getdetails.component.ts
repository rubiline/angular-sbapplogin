import {Component, Input, OnInit, Output, ViewChild} from '@angular/core';
import {FormGroup, Validators} from '@angular/forms';
import {GetaccountService} from '../top-level/process-getaccount/getaccount.service';
import {User} from '../producer';
import {MatStepper, MatTabGroup} from '@angular/material';

@Component({
  selector: 'app-process-getdetails',
  templateUrl: './process-getdetails.component.html',
  styleUrls: ['./process-getdetails.component.css']
})


export class ProcessGetdetailsComponent implements OnInit {
  @Input('group')
    group: FormGroup;
  @Input('stepper')
    stepper: MatStepper;
  @ViewChild(MatTabGroup)
    tab: MatTabGroup;

  name: string;
  state: string;
  license: string;
  ssn: string;

  user: User;
  constructor(private _accserv: GetaccountService) {
  }

  ngOnInit() {
  }

  private submitSsn(): void {
    this.user = this._accserv.checkDetailsSsn(this.name, this.ssn);
  }

  private submitNpn(): void {
    this.user = this._accserv.checkDetailsNpn(this.ssn, this.name);
  }

  private validateInputData(): boolean {
    let result = false;
    if (this.ssn) {
      result = this.ssn.length === 9;
 //     result = this.isLastNameValid && result;
    } else {
 //     result = this.isLicenseValid;
 //     result = this.isStateValid && result;
    }
    return result;
  }

  private nextButton(): void {
    const ssn = this.tab.selectedIndex;
    if (ssn === 0) {
      this.submitSsn();
    } else {
      this.submitNpn();
    }

    if (this.user) {
      console.log('User ' + this.user.NAME + ' found!');
      this._accserv.nextForm(this.stepper);
    } else {
      console.log('User not found.');
    }
  }
}


