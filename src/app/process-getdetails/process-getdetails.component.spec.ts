import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProcessGetdetailsComponent } from './process-getdetails.component';

describe('ProcessGetdetailsComponent', () => {
  let component: ProcessGetdetailsComponent;
  let fixture: ComponentFixture<ProcessGetdetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProcessGetdetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProcessGetdetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
