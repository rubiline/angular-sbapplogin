import {Component, Input, OnInit} from '@angular/core';
import {FormGroup} from '@angular/forms';

@Component({
  selector: 'app-process-dobverification',
  templateUrl: './process-dobverification.component.html',
  styleUrls: ['./process-dobverification.component.css']
})
export class ProcessDobverificationComponent implements OnInit {

  @Input('group')
    group: FormGroup;

  constructor() { }

  ngOnInit() {
  }

}
