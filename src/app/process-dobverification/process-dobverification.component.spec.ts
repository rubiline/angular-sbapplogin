import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProcessDobverificationComponent } from './process-dobverification.component';

describe('ProcessDobverificationComponent', () => {
  let component: ProcessDobverificationComponent;
  let fixture: ComponentFixture<ProcessDobverificationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProcessDobverificationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProcessDobverificationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
