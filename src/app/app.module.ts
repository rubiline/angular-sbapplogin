import { BrowserModule } from '@angular/platform-browser';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { NgModule } from '@angular/core';
import {HttpClient, HttpClientModule, HttpHandler} from '@angular/common/http';

import {MatFormFieldModule } from '@angular/material/form-field';
import {MatInputModule} from '@angular/material';
import {MatSelectModule} from '@angular/material';
import {MatStepperModule} from '@angular/material/stepper';
import {MatCardModule} from '@angular/material/card';
import {MatButtonModule} from '@angular/material/button';
import {MatDividerModule} from '@angular/material/divider';
import {MatTabsModule} from '@angular/material/tabs';

import { AppComponent } from './app.component';
import { ProcessGetaccountComponent } from './top-level/process-getaccount/process-getaccount.component';
import { ContainerComponent } from './top-level/container/container.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ProcessLogindetailsComponent } from './top-level/process-logindetails/process-logindetails.component';

import { NiprConfirmationComponent } from './nipr-confirmation/nipr-confirmation.component';
import { ProcessGetdetailsComponent } from './process-getdetails/process-getdetails.component';
import { ProcessDobverificationComponent } from './process-dobverification/process-dobverification.component';
import {UserService} from './user.service';
import { ProcessReviewComponent } from './top-level/process-review/process-review.component';
import { ProcessActivationComponent } from './top-level/process-activation/process-activation.component';


@NgModule({
  declarations: [
    AppComponent,
    ContainerComponent,
    ProcessGetaccountComponent,
    ProcessLogindetailsComponent,
    NiprConfirmationComponent,
    ProcessGetdetailsComponent,
    ProcessDobverificationComponent,
    ProcessReviewComponent,
    ProcessActivationComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatStepperModule,
    MatCardModule,
    MatButtonModule,
    MatDividerModule,
    MatTabsModule
  ],
  providers: [HttpClient, UserService],
  bootstrap: [AppComponent]
})
export class AppModule { }
