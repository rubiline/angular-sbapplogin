import { Injectable } from '@angular/core';
import {User} from './producer';
import { HttpClient } from '@angular/common/http';
import {catchError} from 'rxjs/operators';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class UserService {

  private readonly http: HttpClient;

  constructor(http: HttpClient) {
    this.http = http;
  }

  user: Observable<User>;

  url = '/api/surecrm/nipr/';

  searchNIPRSsn(surname: string, ssn: string): Observable<User> {
    // send stuff to nipr and return full name string (null if none found)
    // limitedUser should be name, ssn, npn, and STATE_RESIDENT + true or false condition based on success
    console.log(this.url + 'bySSN?ssn=' + ssn + '&lastName=' + surname);
    return this.http.get<User>(this.url + 'bySSN?ssn=' + ssn + '&lastName=' + surname);
  }

  searchNIPRNpn(state: string, npn: string): Observable<User> {
    // send stuff to nipr and return full name string (null if none found)
    // limitedUser should be name, ssn, npn, and STATE_RESIDENT + true or false condition based on success
    return this.http.get<User>(this.url + 'byNPN?npn=' + npn + '&state=' + state);
  }

  loadPdbReport(agency: number, npn: string, ssn: string): Observable<Boolean> {
    return this.http.get<Boolean>(this.url + 'loadPdbReport?agency=' + agency + 'npn=' + npn + '&ssn=' + ssn);
  }

  sentActivationEmail(): void {
    /*
    const params: User;

    var sendTo:String = securityContext.isProducer ? AppEmailMessage.RECEIVER_TYPE_PRODUCER : AppEmailMessage.RECEIVER_TYPE_ADMINISTRATOR;
    var message:AppEmailMessage = buildActivationMessage(securityContext.branchCode, sendTo);
    appEmailService.sendEmail(message);


    return this.http.put<Boolean>(this.url + '/activation', params);
    */
  }
}


// create the skeleton in UI
// implement service
// go to
