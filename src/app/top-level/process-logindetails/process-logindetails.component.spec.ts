import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProcessLogindetailsComponent } from './process-logindetails.component';

describe('ProcessLogindetailsComponent', () => {
  let component: ProcessLogindetailsComponent;
  let fixture: ComponentFixture<ProcessLogindetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProcessLogindetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProcessLogindetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
