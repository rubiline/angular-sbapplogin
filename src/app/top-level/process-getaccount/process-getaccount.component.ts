import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {UserService} from '../../user.service';
import {User} from '../../producer';
import {MatStepper} from '@angular/material';

@Component({
  selector: 'app-process-getaccount',
  templateUrl: './process-getaccount.component.html',
  styleUrls: ['./process-getaccount.component.scss'],
  providers: [UserService]
})
export class ProcessGetaccountComponent implements OnInit {

  user: User;
  surname = '';
  ssn = '';
  state = '';
  npn = '';
  @ViewChild(MatStepper) stepper: MatStepper;

  firstFormGroup: FormGroup;
  secondFormGroup: FormGroup;

  pdbCost = 0;
  pdbVerify = false;

  fullName = '';

  constructor(private _formBuilder: FormBuilder, private _userv: UserService) {
    // initiate form groups
    this.firstFormGroup = this._formBuilder.group({
      ssnCtrl: ['', Validators.required],
      surCtrl: ['', Validators.required],
      licenseCtrl: ['', Validators.required],
      stateCtrl: ['', Validators.required]
    });
    this.secondFormGroup = this._formBuilder.group({
      dobCtrl: ['', Validators.required]
    });
  }

  ngOnInit() {

  }


}
