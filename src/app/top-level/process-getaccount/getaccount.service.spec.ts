import { TestBed, inject } from '@angular/core/testing';

import { GetaccountService } from './getaccount.service';

describe('GetaccountService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [GetaccountService]
    });
  });

  it('should be created', inject([GetaccountService], (service: GetaccountService) => {
    expect(service).toBeTruthy();
  }));
});
