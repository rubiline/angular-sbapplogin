import { Injectable } from '@angular/core';
import {User} from '../../producer';
import {MatStepper} from '@angular/material';
import {UserService} from '../../user.service';
import {of} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class GetaccountService {

  private user: User;

  constructor(private _userv: UserService) { }

  checkDetailsSsn(surname: string, ssn: string): User {
    this._userv.searchNIPRSsn(surname, ssn)
      .subscribe(user => this.user = user);
    return this.user;
  }

  checkDetailsNpn(state: string, npn: string): User {

    this._userv.searchNIPRNpn(state, npn)
      .subscribe(u => this.user = u);
    return this.user;
  }

   isDOBMatch(niprDOB: Date, selectedDOB: Date): boolean {
    let result = false;
    if (niprDOB && selectedDOB) {
      result = niprDOB.toDateString() === selectedDOB.toDateString();
    }
    return result;
  }

  loadPdbReport(npn: string, ssn: string): boolean {
    // pass agency parameter into this
    let r;
    this._userv.loadPdbReport(1, npn, ssn)
      .subscribe(v => r = v);
    return r;
  }

  nextForm(stepper: MatStepper): void {
    console.log('nextForm called!');
    if (true) {
      stepper.next();
    } else {
      // i dunno make a model or something
    }
  }

}
