import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProcessGetaccountComponent } from './process-getaccount.component';

describe('ProcessGetaccountComponent', () => {
  let component: ProcessGetaccountComponent;
  let fixture: ComponentFixture<ProcessGetaccountComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProcessGetaccountComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProcessGetaccountComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
