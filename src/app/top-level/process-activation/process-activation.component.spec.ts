import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProcessActivationComponent } from './process-activation.component';

describe('ProcessActivationComponent', () => {
  let component: ProcessActivationComponent;
  let fixture: ComponentFixture<ProcessActivationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProcessActivationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProcessActivationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
