import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProcessReviewComponent } from './process-review.component';

describe('ProcessReviewComponent', () => {
  let component: ProcessReviewComponent;
  let fixture: ComponentFixture<ProcessReviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProcessReviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProcessReviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
