export class User {
  ID_ENTITY: number;
  STATE_RESIDENT: string;
  DATE_BIRTH: string;
  NPN: string;
  ENTITY_TYPE: string;
  NAME: string;
  // extra stuff when necessary
}
